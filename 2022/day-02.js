console.log("Solution for day 02/12/2022");
console.log("https://adventofcode.com/2022/day/2");
console.log("");

const fs = require('fs');
const path = require('path');

const POINTS_LOST = 0;
const POINTS_DRAW = 3;
const POINTS_WON = 6;

const POINTS_ROCK = 1;
const POINTS_PAPER = 2;
const POINTS_SCISSORS = 3;

function points(shape)
{
    switch(shape)
    {
        case 'A':
        case 'X':
            return POINTS_ROCK;
        case 'B':
        case 'Y':
            return POINTS_PAPER;
        case 'C':
        case 'Z':
            return POINTS_SCISSORS;
        default:
            return null;
    }
}

function wins(shape1, toShape2)
{
    switch(shape1)
    {
        case 'A': // ROCK
        case 'X':
            switch(toShape2)
            {
                case 'A': // ROCK
                case 'X':
                    return POINTS_DRAW;
                case 'B': // PAPER
                case 'Y':
                    return POINTS_LOST;
                case 'C': // SCISSORS
                case 'Z':
                    return POINTS_WON;
            }
        case 'B': //PAPER
        case 'Y':
            switch(toShape2)
            {
                case 'A': // ROCK
                case 'X':
                    return POINTS_WON;
                case 'B': // PAPER
                case 'Y':
                    return POINTS_DRAW;
                case 'C': // SCISSORS
                case 'Z':
                    return POINTS_LOST;
            }
        case 'C': //SCISSORS
        case 'Z':
            switch(toShape2)
            {
                case 'A': // ROCK
                case 'X':
                    return POINTS_LOST;
                case 'B': // PAPER
                case 'Y':
                    return POINTS_WON;
                case 'C': // SCISSORS
                case 'Z':
                    return POINTS_DRAW;
            }
    }

    return null;
}

function readInput()
{
    const ret = [];

    const filePath = path.join(__dirname, 'input-02.txt');
    const file = fs.readFileSync(filePath, 'utf-8');
    file.split(/\r?\n/).forEach(line =>
    {
        ret.push({other: line[0], you: line[2]});
    });
    return ret;
}

const input = readInput();

let calculatedPoints = 0;
input.forEach(move => {
    calculatedPoints += points(move.you);
    calculatedPoints += wins(move.you, move.other);
});

console.log("Respuesta parte #1: " + calculatedPoints);

//-----------------------------------------------------

function whatWinsTo(shape)
{
    switch(shape)
    {
        case 'A': // ROCK
            return 'B';
        case 'B': // PAPER
            return 'C';
        case 'C': // SCISSORS
            return 'A';
        default:
            return null;
    }
}

function whatLosesTo(shape)
{
    switch(shape)
    {
        case 'A': // ROCK
            return 'C';
        case 'B': // PAPER
            return 'A';
        case 'C': // SCISSORS
            return 'B';
        default:
            return null;
    }
}

function chooseYourShape(move)
{
    switch(move.you)
    {
        case 'X': // Lose
            return whatLosesTo(move.other);
        case 'Y': // draw
            return move.other;
        case 'Z': // win
            return whatWinsTo(move.other);
        default:
            return null;
    }
}

let calculatedPoints2 = 0;
input.forEach(move => {

    let yourShape = chooseYourShape(move);
    calculatedPoints2 += points(yourShape);
    calculatedPoints2 += wins(yourShape, move.other);
});

console.log("Respuesta parte #2: " + calculatedPoints2);