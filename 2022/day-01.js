console.log("Solution for day 01/12/2022");
console.log("https://adventofcode.com/2022/day/1");
console.log("");

const fs = require('fs');
const path = require('path');

function readInput()
{
    const ret = [];
    let currLine = [];

    const filePath = path.join(__dirname, 'input-01.txt');
    const file = fs.readFileSync(filePath, 'utf-8');
    file.split(/\r?\n/).forEach(line =>
    {
        if(line) {
            currLine.push(parseInt(line));
        } else {
            currLine = [];
            ret.push(currLine);
        }
    });
    ret.push(currLine);
    return ret;
}

const input = readInput();
const caloriesList = [];

let maxCalories = 0;
for(let i = 0; i < input.length; i++)
{
    let currentCalories = 0;
    for(let j = 0; j < input[i].length; j++)
    {
        currentCalories += input[i][j];
    }

    caloriesList.push(currentCalories);
    maxCalories = Math.max(maxCalories, currentCalories);
}

console.log("Respuesta parte #1: " + maxCalories);

//Parte 2

caloriesList.sort((a,b) => { return b - a; });
const maxThreeCalories = caloriesList[0] + caloriesList[1] + caloriesList[2];

console.log("Respuesta parte #2: " + maxThreeCalories);
