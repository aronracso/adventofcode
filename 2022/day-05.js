console.log("Solution for day 05/12/2022");
console.log("https://adventofcode.com/2022/day/5");
console.log("");

const fs = require('fs');
const path = require('path');

function readInput()
{
    const ret = [];
    const filePath = path.join(__dirname, 'input-05.txt');
    const file = fs.readFileSync(filePath, 'utf-8');
    file.split(/\r?\n/).forEach(line =>
    {
        ret.push(line);
    });
    return ret;
}

function parseStackState(input)
{
/*//Example:
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 
*/
    const stacks = [];

    // Find stacks number (last number, last line)
    const lastLine = input[input.length - 1];
    const stackCount = parseInt(lastLine[lastLine.length - 2]);

    //Init stacks
    for(let i = 0; i < stackCount; i++) {
        stacks.push([]);
    }

    //Read stacks lines in reverse order
    for(let i = input.length - 2; i >= 0; i--)
    {
        //Read each stack
        const line = input[i];
        for(let j = 0; j < stackCount; j++)
        {
            let letter = line[(j*4) + 1];
            if(letter === ' ')
                continue;

            stacks[j].push(letter);
        }
    }

    return stacks;
}

function parseMoves(input)
{
    //Example:
    //move 3 from 2 to 9
    const regex = new RegExp(/move (\d+) from (\d+) to (\d+)/);

    const moves = [];

    input.forEach(line => {
        let match = regex.exec(line);
        moves.push({
            move: match[1],
            from: match[2],
            to: match[3]
        })
    });

    return moves;
}

function excecuteMove(stacks, move)
{
    for(let i = 0; i < move.move; i++)
    {
        let item = stacks[move.from - 1].pop();
        stacks[move.to - 1].push(item);
    }
}

function extractStackHeads(stacks)
{
    let ret = "";
    for(let i = 0; i < stacks.length; i++)
    {
        ret += stacks[i][stacks[i].length - 1];
    }
    return ret;
}

const input = readInput();

const inputSeparatorIndex = input.indexOf('');

let stacks = parseStackState(input.slice(0, inputSeparatorIndex));
const moves = parseMoves(input.slice(inputSeparatorIndex + 1));

moves.forEach(move => {
    excecuteMove(stacks, move);
});

const message1 = extractStackHeads(stacks);
console.log("Response part #1: " + message1);

//-----------------------------------------------------

stacks = parseStackState(input.slice(0, inputSeparatorIndex));

function excecuteMove2(stacks, move)
{
    let items = stacks[move.from - 1].splice(-1 * move.move);

    //INFO: If I try to 'push' items, it inserts the array, not the elements
    for(let i = 0; i < items.length; i++)
        stacks[move.to - 1].push(items[i]);
}

moves.forEach(move => {
    excecuteMove2(stacks, move);
});

const message2 = extractStackHeads(stacks);
console.log("Response part #2: " + message2);

