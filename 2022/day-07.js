console.log("Solution for day 07/12/2022");
console.log("https://adventofcode.com/2022/day/7");
console.log("");

const fs = require('fs');
const path = require('path');

function readInput()
{
    const ret = [];
    const filePath = path.join(__dirname, 'input-07.txt');
    const file = fs.readFileSync(filePath, 'utf-8');
    file.split(/\r?\n/).forEach(line =>
    {
        ret.push(line);
    });
    return ret;
}

//-----------------------------------------------------
// File System Elements:

const FSDIR = "dir";
const FSFILE = "file";

class FSElement {
    constructor(type, name, size) {
        this.type = type;
        this.name = name;
        size = size ?? 0;
        this.size = parseInt(size);
        this.dirSize = null;

        this.parent = null;
        this.childs = [];
    }

    addChild(element) {
        element.parent = this;
        this.childs.push(element);
        this[element.name] = element;
    }

    calculateSize() {
        if(this.type === FSFILE)
            return this.size;
        else if(this.dirSize === null) {
            this.dirSize = 0;
            for(let i = 0; i < this.childs.length; i++)
                this.dirSize += this.childs[i].calculateSize();
            return this.dirSize;
        } else {
            return this.dirSize;
        }
    }

    toString() {
        return this.name + " (" + this.type +
            (this.type === FSFILE ? (", size=" + this.size) : "") +
            (this.type === FSDIR ? (", dirSize=" + this.calculateSize()) : "") +
            ")";
    }
}


function printFileSystem(node, level)
{
    level = level ?? 0;

    let line = "";
    for(let i = 0; i < level; i++)
        line += "  ";

    line += "- " + node.toString();
    console.info(line);

    for(let i = 0; i < node.childs.length; i++)
    {
        printFileSystem(node.childs[i], level + 1);
    }
}

const FSROOTNAME = "/";
const root = new FSElement(FSDIR, FSROOTNAME);

//-----------------------------------------------------
// (cd) Change directory moves:

let currentDirectory = null;

function executeCDCommand(i, input)
{
    let line = input[i];
    let path = line.split(" ")[2];

    switch(path) {
        case "..": //move out
            currentDirectory = currentDirectory.parent;
            break;
        case FSROOTNAME: //nove to root
            currentDirectory = root;
            break;
        default: //move in
            currentDirectory = currentDirectory[path];
            break;
    }

    return i + 1;
}

//-----------------------------------------------------
// (ls) List File System:

function createDirectory(name)
{
    let dir = new FSElement(FSDIR, name, 0);
    currentDirectory.addChild(dir);
}

function createFile(name, size)
{
    let file = new FSElement(FSFILE, name, size);
    currentDirectory.addChild(file);
}

function executeLSCommand(i, input)
{
    i += 1;

    let line = input[i];
    while(line && !line.startsWith(COMMAND_START))
    {
        let lineSplit = line.split(" ");

        if(lineSplit[0] === FSDIR) {
            createDirectory(lineSplit[1]);
        } else {
            createFile(lineSplit[1], lineSplit[0]);
        }

        i += 1;
        line = input[i];
    }

    return i;
}

//-----------------------------------------------------
// Commands:

const COMMAND_START = "$";

const commands = {
    'cd': executeCDCommand,
    'ls': executeLSCommand
};

const inputCommands = readInput();

let commLine = 0;
do
{
    let line = inputCommands[commLine];
    if(line.startsWith(COMMAND_START))
    {
        let lineSplit = line.split(" ");
        commLine = commands[lineSplit[1]](commLine, inputCommands);
    }
    else
    {
        commLine += 1;
    }
}
while(commLine < inputCommands.length);

printFileSystem(root);
console.log("");

//-----------------------------------------------------
// Part 1: Read sizes at most 100000 size

function iterateFileSystem_findMost100k(node)
{
    let totalSize = 0;

    if(node.type === FSDIR)
    {
        let mySize = node.calculateSize();
        if(mySize < 100000)
            totalSize += mySize;
    }

    for(let i = 0; i < node.childs.length; i++)
    {
        totalSize += iterateFileSystem_findMost100k(node.childs[i]);
    }

    return totalSize;
}

const response = iterateFileSystem_findMost100k(root);
console.log("Response part #1: " + response);
console.log("");

//-----------------------------------------------------
// Part 2: Choose directory to delete

function extractAllDirectories(node)
{
    const ret = [];

    for(let i = 0; i < node.childs.length; i++)
    {
        let child = node.childs[i];
        if(child.type === FSDIR)
            ret.push(child);

        let others = extractAllDirectories(child);
        for(let j = 0; j < others.length; j++)
            ret.push(others[j]);
    }

    return ret;
}

const allDirectories = extractAllDirectories(root);
allDirectories.sort((a, b) => { return a.dirSize - b.dirSize; });

const maxSizeToUse = 70000000 - 30000000;
let sizeToDelete = root.dirSize - maxSizeToUse;
let dirToDelete = null;

for(let i = 0; i < allDirectories.length; i++)
{
    let dir = allDirectories[i];
    if(dir.dirSize >= sizeToDelete) {
        dirToDelete = dir;
        break;
    }
}

console.log("Response part #2: " + dirToDelete.dirSize);
