console.log("Solution for day 04/12/2022");
console.log("https://adventofcode.com/2022/day/4");
console.log("");

const fs = require('fs');
const path = require('path');

function readInput()
{
    const ret = [];
    const filePath = path.join(__dirname, 'input-04.txt');
    const file = fs.readFileSync(filePath, 'utf-8');
    file.split(/\r?\n/).forEach(line =>
    {
        ret.push(line);
    });
    return ret;
}

function parseInput(lines)
{
    const ret = [];
    for(let i = 0; i < lines.length; i++)
    {
        const elftSections = lines[i].split(",");
        const section1 = elftSections[0].split("-");
        const section2 = elftSections[1].split("-");

        ret.push(
            [{
                start: parseInt(section1[0]),
                end: parseInt(section1[1])
            },{
                start: parseInt(section2[0]),
                end: parseInt(section2[1])
            }]
        );
    }
    return ret;
}

function isFullOverlaped(sect1, sect2)
{
    return (sect1.start <= sect2.start && sect1.end >= sect2.end) ||
        (sect2.start <= sect1.start && sect2.end >= sect1.end);
}

const input = parseInput(readInput());

let fillOverlapedSections = 0;
input.forEach(sections => {
    if(isFullOverlaped(sections[0], sections[1]))
        fillOverlapedSections += 1;
});

console.log("Response part #1: " + fillOverlapedSections);

//-----------------------------------------------------


function isSomeOverlaped(sect1, sect2)
{
    return !(sect2.end < sect1.start || sect1.end < sect2.start);
}

let someOverlapedSections = 0;
input.forEach(sections => {
    if(isSomeOverlaped(sections[0], sections[1]))
        someOverlapedSections += 1;
});

console.log("Response part #2: " + someOverlapedSections);