console.log("Solution for day 06/12/2022");
console.log("https://adventofcode.com/2022/day/6");
console.log("");

const fs = require('fs');
const path = require('path');

function readInput()
{
    const ret = [];
    const filePath = path.join(__dirname, 'input-06.txt');
    const file = fs.readFileSync(filePath, 'utf-8');
    file.split(/\r?\n/).forEach(line =>
    {
        ret.push(line);
    });
    return ret;
}

function isStartMarker(buffer)
{
    for(let i = 0; i < buffer.length; i++) {
        for(let j = i + 1; j < buffer.length; j++) {
            if(buffer[i] === buffer[j])
                return false;
        }
    }

    return true;
}

function findMarker(input, bufferSize)
{
    const buffer = [];
    for(let i = 0; i < bufferSize; i++) {
        buffer.push(input[i]);
    }
    
    let response = null;
    for(let i = bufferSize; i < input.length; i++)
    {
        let char = input[i];
        buffer.push(char);
    
        if(isStartMarker(buffer)) {
            response = (i+1);
            break;
        }
    
        buffer.shift();
    }

    return response;
}


const input = readInput()[0];


const response = findMarker(input, 3);
console.log("Response part #1: " + response);

//-----------------------------------------------------

const response2 = findMarker(input, 13);
console.log("Response part #2: " + response2);
