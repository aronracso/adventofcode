console.log("Solution for day 03/12/2022");
console.log("https://adventofcode.com/2022/day/3");
console.log("");

const fs = require('fs');
const path = require('path');

function readInput()
{
    const ret = [];
    const filePath = path.join(__dirname, 'input-03.txt');
    const file = fs.readFileSync(filePath, 'utf-8');
    file.split(/\r?\n/).forEach(line =>
    {
        ret.push(line);
    });
    return ret;
}

const PRIORITY = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
function calculatePriority(item)
{
    return PRIORITY.indexOf(item) + 1;
}

function findDuplicatedItem(line)
{
    let half = line.length / 2;
    let first = line.slice(0, half);
    let second = line.slice(half);

    for(let f = 0; f < first.length; f++)
    {
        let firstItem = first[f];
        for(let s = 0; s < second.length; s++)
        {
            if(firstItem === second[s])
                return firstItem;
        }
    }

    throw new 'Item not found';
}

const input = readInput();

let sumPriority = 0;
for(let i = 0; i < input.length; i++)
{
    const duplicatedItem = findDuplicatedItem(input[i]);
    sumPriority += calculatePriority(duplicatedItem);
}

console.log("Respuesta parte #1: " + sumPriority);

//-----------------------------------------------------

function findAllDuplicatedItems2(first, second)
{
    let ret = "";
    for(let f = 0; f < first.length; f++)
    {
        let firstItem = first[f];
        for(let s = 0; s < second.length; s++)
        {
            if(firstItem === second[s])
                ret += firstItem;
        }
    }
    return ret;
}

function findDuplicatedItem2(lines)
{
    let duplicated = findAllDuplicatedItems2(lines[0], lines[1]);

    for(let i = 2; i < lines.length; i++)
    {
        duplicated = findAllDuplicatedItems2(duplicated, lines[i]);
    }

    return duplicated.charAt(0);
}

let sumPriority2 = 0;
for(let i = 0; i < input.length; i += 3)
{
    const duplicatedItem = findDuplicatedItem2(input.slice(i, i + 3));
    sumPriority2 += calculatePriority(duplicatedItem);
}

console.log("Respuesta parte #2: " + sumPriority2);
