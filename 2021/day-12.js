console.log("Solution for day 12/12/2021");
console.log("https://adventofcode.com/2021/day/12");
console.log("");


function constructNodes()
{
    const nodes = {};
    INPUT.forEach(item =>
    {
        item = item.split('-');
        item.from = item[0];
        item.to = item[1];

        let nodeFrom = nodes[item.from];
        if(!nodeFrom) {
            nodeFrom = [];
            nodes[item.from] = nodeFrom;
        }
        if(item.from !== 'end' && item.to !== 'end')
            nodeFrom.push(item.to);

        let nodeTo = nodes[item.to];
        if(!nodeTo) {
            nodeTo = [];
            nodes[item.to] = nodeTo;
        }
        if(item.to !== 'end' && item.to !== 'start' && item.from !== 'start')
            nodeTo.push(item.from);
    });
    return nodes;
}

function resolve()
{
    const nodes = constructNodes();
    console.log(JSON.stringify(nodes));



    let resp1 = null;
    console.log(`SOLUTION FOR 12.1: ${resp1}`);
}

const INPUT =
[
    ///* Small test  --> 10 paths
    "start-A",
    "start-b",
    "A-c",
    "A-b",
    "b-d",
    "A-end",
    "b-end",
    //*/

    /* //Test real
    { "from": "start", "to": "YA" },
    { "from": "ps", "to": "yq" },
    { "from": "zt", "to": "mu" },
    { "from": "JS", "to": "yi" },
    { "from": "yq", "to": "VJ" },
    { "from": "QT", "to": "ps" },
    { "from": "start", "to": "yq" },
    { "from": "YA", "to": "yi" },
    { "from": "start", "to": "nf" },
    { "from": "nf", "to": "YA" },
    { "from": "nf", "to": "JS" },
    { "from": "JS", "to": "ez" },
    { "from": "yq", "to": "JS" },
    { "from": "ps", "to": "JS" },
    { "from": "ps", "to": "yi" },
    { "from": "yq", "to": "nf" },
    { "from": "QT", "to": "yi" },
    { "from": "end", "to": "QT" },
    { "from": "nf", "to": "yi" },
    { "from": "zt", "to": "QT" },
    { "from": "end", "to": "ez" },
    { "from": "yq", "to": "YA" },
    { "from": "end", "to": "JS" },
    //*/
];

resolve();