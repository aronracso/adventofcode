console.log("Solution for day 11/12/2021");
console.log("https://adventofcode.com/2021/day/11");
console.log("");

const DELTAS = [
    { "x": 0, "y": 1},
    { "x": 1, "y": 1},
    { "x": 1, "y": 0},
    { "x": 1, "y": -1},
    { "x": 0, "y": -1},
    { "x": -1, "y": -1},
    { "x": -1, "y": 0},
    { "x": -1, "y": 1},
];

function resolve()
{
    let flashes = 0;

    let maxy = INPUT.length;
    let maxx = INPUT[0].length;

    inputToConsole();

    for(let i = 0; i < 100; i++)
    {
        addOneToAll(maxx, maxy);
        flashes += resetAndGetFlashes();

        console.debug(`${i}) flashes=${flashes}`);
        inputToConsole();
    }

    console.debug();
    console.debug("FINAL: ");
    inputToConsole();
    console.log(`SOLUTION FOR 11.1: ${flashes}`);

    let cont = 100;
    while(!checkAllZero())
    {
        addOneToAll(maxx, maxy);
        resetAndGetFlashes();
        cont += 1;
    }
    console.debug();
    console.debug("FINAL2: ");
    inputToConsole();
    console.log(`SOLUTION FOR 11.2: ${cont}`);
}

function isOutside(x, y, maxx, maxy)
{
    return x < 0 || x >= maxx || y < 0 || y >= maxy;
}

function getPoint(x, y, maxx, maxy)
{
    if(isOutside(x, y, maxx, maxy))
        return null;

    return INPUT[y][x];
}

function addOneToAll(maxx, maxy)
{
    let addExtraQueue = [];

    for(let y = 0; y < INPUT.length; y++)
    {
        for(let x = 0; x < INPUT[y].length; x++)
        {
            INPUT[y][x] += 1;
            if(INPUT[y][x] > 9)
                addExtraQueue.push({ "x": x, "y": y});
        }
    }
    
    //inputToConsole();

    while(addExtraQueue.length > 0)
    {
        let pos = addExtraQueue.shift();
        for(let d = 0; d < DELTAS.length; d++)
        {
            let x = pos.x + DELTAS[d].x;
            let y = pos.y + DELTAS[d].y;
            if(isOutside(x, y, maxx, maxy))
                continue;

            if(INPUT[y][x] > 9)
                continue;
            
            INPUT[y][x] += 1;

            //console.debug();
            //console.debug(`added one (${pos.x},${pos.y} + ${d}) to [${x},${y}] = ${INPUT[y][x]}`);
            //inputToConsole();

            if(INPUT[y][x] > 9)
                addExtraQueue.push({ "x": x, "y": y});
        }
    }
}

function resetAndGetFlashes()
{
    let flashes = 0;
    for(let y = 0; y < INPUT.length; y++)
    {
        for(let x = 0; x < INPUT[y].length; x++)
        {
            if(INPUT[y][x] > 9)
            {
                INPUT[y][x] = 0;
                flashes += 1;
            }
        }
    }

    return flashes;
}

function checkAllZero()
{
    for(let y = 0; y < INPUT.length; y++)
    {
        for(let x = 0; x < INPUT[y].length; x++)
        {
            if(INPUT[y][x] !== 0)
                return false;
        }
    }
    return true;
}

function inputToConsole()
{
    let str = "";
    for(let y = 0; y < INPUT.length; y++)
    {
        for(let x = 0; x < INPUT[y].length; x++)
        {
            if(INPUT[y][x] > 9)
                str += "X";
            else
                str += INPUT[y][x];
        }
        str += "\r\n";
    }
    console.debug(str);
}

const INPUT =
[
    /* //Test 0.5
    [5,4,8,3,1],
    [2,7,4,5,8],
    [5,2,6,4,5],
    [6,1,4,1,3],
    //*/

    /* //Test 1
    [5,4,8,3,1,4,3,2,2,3,],
    [2,7,4,5,8,5,4,7,1,1,],
    [5,2,6,4,5,5,6,1,7,3,],
    [6,1,4,1,3,3,6,1,4,6,],
    [6,3,5,7,3,8,5,4,7,8,],
    [4,1,6,7,5,2,4,6,4,5,],
    [2,1,7,6,8,4,1,7,2,1,],
    [6,8,8,2,8,8,1,1,3,4,],
    [4,8,4,6,8,4,8,5,5,4,],
    [5,2,8,3,7,5,1,5,2,6,]
    //*/

    ///* //Real test
    [4,4,7,2,5,6,2,2,6,4,],
    [8,6,3,1,5,1,7,8,2,7,],
    [7,2,3,2,1,4,4,1,4,6,],
    [2,4,4,7,1,6,3,8,2,4,],
    [1,2,3,5,2,7,2,6,7,1,],
    [5,1,3,3,5,2,7,1,4,6,],
    [6,5,1,1,3,7,2,4,1,7,],
    [3,8,4,1,8,4,1,6,1,4,],
    [8,6,2,1,3,6,8,7,8,2,],
    [3,2,4,6,3,3,6,6,7,7,]
    //*/
];

resolve();