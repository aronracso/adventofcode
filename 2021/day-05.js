console.log("Solution for day 5/12/2021");
console.log("https://adventofcode.com/2021/day/5");
console.log("");

class MapMatrix
{
    constructor()
    {
        this.matrix = {};
        this.maxx = 0;
        this.maxy = 0;
        this.numOverlap = 0;
    }

    coords(x, y)
    {
        return x + "," + y;
    }

    markVHLine(line)
    {
        //check vertical or horizontal
        if(!(line.x1 === line.x2 || line.y1 === line.y2))
            return;

        this.markLine(line);
    }

    markLine(line)
    {
        let deltax = 0;
        if(line.x1 < line.x2)
            deltax = 1;
        else if(line.x1 > line.x2)
            deltax = -1;

        let deltay = 0;
        if(line.y1 < line.y2)
            deltay = 1;
        else if(line.y1 > line.y2)
            deltay = -1;

        let posx = line.x1;
        let posy = line.y1;

        this.set(posx, posy);
        do 
        {
            posx += deltax;
            posy += deltay;

            this.set(posx, posy);
        } while(!(posx === line.x2 && posy === line.y2))
    }

    set(x, y)
    {
        this.maxx = Math.max(this.maxx, x);
        this.maxy = Math.max(this.maxy, y);

        let pos = this.get(x, y);
        this.matrix[this.coords(x, y)] = (pos + 1);

        if(pos === 1)
            this.numOverlap += 1;
    }

    get(x, y)
    {
        let pos = this.matrix[this.coords(x, y)];
        if(pos !== undefined)
            return pos;
        return 0;
    }
}

function resolve()
{
    let map1 = new MapMatrix();
    INPUT.forEach(line => {
        map1.markVHLine(line);
    });
    
    console.log(`SOLUTION FOR 5.1: ${map1.numOverlap}`);

    let map2 = new MapMatrix();
    INPUT.forEach(line => {
        map2.markLine(line);
    });
    
    console.log(`SOLUTION FOR 5.2: ${map2.numOverlap}`);
}

const INPUT = [
{ 'x1': 818, 'y1': 513, 'x2': 818, 'y2': 849 },
{ 'x1': 259, 'y1': 377, 'x2': 259, 'y2': 599 },
{ 'x1': 120, 'y1': 758, 'x2': 977, 'y2': 758 },
{ 'x1': 49, 'y1': 386, 'x2': 170, 'y2': 386 },
{ 'x1': 773, 'y1': 644, 'x2': 773, 'y2': 745 },
{ 'x1': 510, 'y1': 958, 'x2': 797, 'y2': 671 },
{ 'x1': 480, 'y1': 438, 'x2': 14, 'y2': 904 },
{ 'x1': 475, 'y1': 346, 'x2': 648, 'y2': 173 },
{ 'x1': 620, 'y1': 167, 'x2': 477, 'y2': 310 },
{ 'x1': 632, 'y1': 756, 'x2': 275, 'y2': 756 },
{ 'x1': 225, 'y1': 896, 'x2': 432, 'y2': 896 },
{ 'x1': 582, 'y1': 450, 'x2': 93, 'y2': 450 },
{ 'x1': 402, 'y1': 262, 'x2': 254, 'y2': 410 },
{ 'x1': 915, 'y1': 236, 'x2': 709, 'y2': 236 },
{ 'x1': 338, 'y1': 530, 'x2': 338, 'y2': 375 },
{ 'x1': 243, 'y1': 857, 'x2': 370, 'y2': 984 },
{ 'x1': 32, 'y1': 449, 'x2': 32, 'y2': 26 },
{ 'x1': 672, 'y1': 832, 'x2': 203, 'y2': 363 },
{ 'x1': 426, 'y1': 961, 'x2': 426, 'y2': 402 },
{ 'x1': 108, 'y1': 469, 'x2': 550, 'y2': 27 },
{ 'x1': 567, 'y1': 698, 'x2': 495, 'y2': 770 },
{ 'x1': 25, 'y1': 872, 'x2': 861, 'y2': 36 },
{ 'x1': 12, 'y1': 15, 'x2': 960, 'y2': 963 },
{ 'x1': 986, 'y1': 159, 'x2': 986, 'y2': 765 },
{ 'x1': 912, 'y1': 437, 'x2': 714, 'y2': 437 },
{ 'x1': 476, 'y1': 400, 'x2': 476, 'y2': 873 },
{ 'x1': 125, 'y1': 52, 'x2': 951, 'y2': 878 },
{ 'x1': 633, 'y1': 939, 'x2': 633, 'y2': 641 },
{ 'x1': 12, 'y1': 986, 'x2': 987, 'y2': 11 },
{ 'x1': 783, 'y1': 204, 'x2': 467, 'y2': 520 },
{ 'x1': 547, 'y1': 834, 'x2': 413, 'y2': 700 },
{ 'x1': 879, 'y1': 133, 'x2': 879, 'y2': 890 },
{ 'x1': 409, 'y1': 128, 'x2': 365, 'y2': 128 },
{ 'x1': 913, 'y1': 404, 'x2': 913, 'y2': 598 },
{ 'x1': 798, 'y1': 40, 'x2': 841, 'y2': 40 },
{ 'x1': 279, 'y1': 410, 'x2': 279, 'y2': 758 },
{ 'x1': 935, 'y1': 807, 'x2': 959, 'y2': 783 },
{ 'x1': 389, 'y1': 391, 'x2': 389, 'y2': 38 },
{ 'x1': 271, 'y1': 736, 'x2': 271, 'y2': 865 },
{ 'x1': 70, 'y1': 10, 'x2': 814, 'y2': 10 },
{ 'x1': 694, 'y1': 266, 'x2': 694, 'y2': 92 },
{ 'x1': 860, 'y1': 128, 'x2': 860, 'y2': 966 },
{ 'x1': 755, 'y1': 871, 'x2': 608, 'y2': 871 },
{ 'x1': 519, 'y1': 54, 'x2': 519, 'y2': 833 },
{ 'x1': 817, 'y1': 733, 'x2': 156, 'y2': 72 },
{ 'x1': 553, 'y1': 558, 'x2': 726, 'y2': 558 },
{ 'x1': 152, 'y1': 21, 'x2': 830, 'y2': 699 },
{ 'x1': 529, 'y1': 721, 'x2': 608, 'y2': 721 },
{ 'x1': 744, 'y1': 719, 'x2': 744, 'y2': 221 },
{ 'x1': 962, 'y1': 966, 'x2': 10, 'y2': 14 },
{ 'x1': 863, 'y1': 813, 'x2': 863, 'y2': 984 },
{ 'x1': 138, 'y1': 167, 'x2': 801, 'y2': 830 },
{ 'x1': 926, 'y1': 901, 'x2': 926, 'y2': 769 },
{ 'x1': 170, 'y1': 630, 'x2': 623, 'y2': 177 },
{ 'x1': 281, 'y1': 207, 'x2': 510, 'y2': 207 },
{ 'x1': 41, 'y1': 52, 'x2': 945, 'y2': 956 },
{ 'x1': 100, 'y1': 204, 'x2': 100, 'y2': 43 },
{ 'x1': 177, 'y1': 588, 'x2': 469, 'y2': 588 },
{ 'x1': 946, 'y1': 37, 'x2': 201, 'y2': 782 },
{ 'x1': 669, 'y1': 89, 'x2': 669, 'y2': 868 },
{ 'x1': 945, 'y1': 32, 'x2': 46, 'y2': 931 },
{ 'x1': 673, 'y1': 274, 'x2': 412, 'y2': 274 },
{ 'x1': 424, 'y1': 979, 'x2': 169, 'y2': 979 },
{ 'x1': 774, 'y1': 220, 'x2': 774, 'y2': 571 },
{ 'x1': 684, 'y1': 272, 'x2': 229, 'y2': 272 },
{ 'x1': 621, 'y1': 458, 'x2': 621, 'y2': 884 },
{ 'x1': 222, 'y1': 964, 'x2': 222, 'y2': 608 },
{ 'x1': 953, 'y1': 146, 'x2': 145, 'y2': 954 },
{ 'x1': 442, 'y1': 736, 'x2': 442, 'y2': 230 },
{ 'x1': 471, 'y1': 341, 'x2': 471, 'y2': 124 },
{ 'x1': 632, 'y1': 954, 'x2': 968, 'y2': 954 },
{ 'x1': 492, 'y1': 562, 'x2': 911, 'y2': 981 },
{ 'x1': 485, 'y1': 320, 'x2': 485, 'y2': 349 },
{ 'x1': 888, 'y1': 758, 'x2': 888, 'y2': 23 },
{ 'x1': 271, 'y1': 555, 'x2': 732, 'y2': 555 },
{ 'x1': 932, 'y1': 415, 'x2': 792, 'y2': 275 },
{ 'x1': 399, 'y1': 478, 'x2': 399, 'y2': 472 },
{ 'x1': 543, 'y1': 774, 'x2': 543, 'y2': 261 },
{ 'x1': 422, 'y1': 872, 'x2': 422, 'y2': 715 },
{ 'x1': 524, 'y1': 471, 'x2': 445, 'y2': 471 },
{ 'x1': 573, 'y1': 513, 'x2': 573, 'y2': 762 },
{ 'x1': 721, 'y1': 39, 'x2': 482, 'y2': 39 },
{ 'x1': 379, 'y1': 482, 'x2': 379, 'y2': 479 },
{ 'x1': 70, 'y1': 770, 'x2': 336, 'y2': 770 },
{ 'x1': 797, 'y1': 724, 'x2': 797, 'y2': 364 },
{ 'x1': 498, 'y1': 646, 'x2': 498, 'y2': 410 },
{ 'x1': 756, 'y1': 19, 'x2': 756, 'y2': 83 },
{ 'x1': 901, 'y1': 918, 'x2': 11, 'y2': 918 },
{ 'x1': 572, 'y1': 23, 'x2': 362, 'y2': 23 },
{ 'x1': 814, 'y1': 563, 'x2': 750, 'y2': 627 },
{ 'x1': 279, 'y1': 736, 'x2': 249, 'y2': 706 },
{ 'x1': 509, 'y1': 257, 'x2': 509, 'y2': 897 },
{ 'x1': 127, 'y1': 374, 'x2': 399, 'y2': 102 },
{ 'x1': 421, 'y1': 139, 'x2': 607, 'y2': 139 },
{ 'x1': 79, 'y1': 520, 'x2': 166, 'y2': 607 },
{ 'x1': 491, 'y1': 779, 'x2': 284, 'y2': 986 },
{ 'x1': 395, 'y1': 606, 'x2': 395, 'y2': 164 },
{ 'x1': 958, 'y1': 244, 'x2': 442, 'y2': 760 },
{ 'x1': 493, 'y1': 509, 'x2': 673, 'y2': 509 },
{ 'x1': 76, 'y1': 321, 'x2': 482, 'y2': 727 },
{ 'x1': 903, 'y1': 926, 'x2': 103, 'y2': 126 },
{ 'x1': 729, 'y1': 689, 'x2': 63, 'y2': 23 },
{ 'x1': 956, 'y1': 13, 'x2': 198, 'y2': 771 },
{ 'x1': 973, 'y1': 452, 'x2': 193, 'y2': 452 },
{ 'x1': 282, 'y1': 362, 'x2': 896, 'y2': 976 },
{ 'x1': 68, 'y1': 985, 'x2': 985, 'y2': 68 },
{ 'x1': 148, 'y1': 109, 'x2': 310, 'y2': 109 },
{ 'x1': 852, 'y1': 920, 'x2': 371, 'y2': 439 },
{ 'x1': 334, 'y1': 41, 'x2': 864, 'y2': 571 },
{ 'x1': 258, 'y1': 592, 'x2': 72, 'y2': 778 },
{ 'x1': 259, 'y1': 990, 'x2': 259, 'y2': 308 },
{ 'x1': 480, 'y1': 363, 'x2': 480, 'y2': 751 },
{ 'x1': 943, 'y1': 153, 'x2': 801, 'y2': 153 },
{ 'x1': 755, 'y1': 172, 'x2': 123, 'y2': 804 },
{ 'x1': 355, 'y1': 501, 'x2': 437, 'y2': 501 },
{ 'x1': 666, 'y1': 769, 'x2': 340, 'y2': 769 },
{ 'x1': 982, 'y1': 10, 'x2': 23, 'y2': 969 },
{ 'x1': 886, 'y1': 410, 'x2': 886, 'y2': 669 },
{ 'x1': 454, 'y1': 410, 'x2': 454, 'y2': 218 },
{ 'x1': 426, 'y1': 688, 'x2': 227, 'y2': 688 },
{ 'x1': 969, 'y1': 959, 'x2': 207, 'y2': 959 },
{ 'x1': 586, 'y1': 27, 'x2': 591, 'y2': 22 },
{ 'x1': 852, 'y1': 133, 'x2': 166, 'y2': 819 },
{ 'x1': 587, 'y1': 199, 'x2': 781, 'y2': 199 },
{ 'x1': 78, 'y1': 85, 'x2': 78, 'y2': 394 },
{ 'x1': 468, 'y1': 162, 'x2': 727, 'y2': 162 },
{ 'x1': 261, 'y1': 473, 'x2': 573, 'y2': 473 },
{ 'x1': 924, 'y1': 300, 'x2': 924, 'y2': 873 },
{ 'x1': 651, 'y1': 817, 'x2': 651, 'y2': 600 },
{ 'x1': 717, 'y1': 909, 'x2': 717, 'y2': 187 },
{ 'x1': 898, 'y1': 656, 'x2': 454, 'y2': 656 },
{ 'x1': 805, 'y1': 873, 'x2': 507, 'y2': 873 },
{ 'x1': 529, 'y1': 825, 'x2': 529, 'y2': 217 },
{ 'x1': 578, 'y1': 503, 'x2': 783, 'y2': 503 },
{ 'x1': 929, 'y1': 663, 'x2': 920, 'y2': 663 },
{ 'x1': 332, 'y1': 938, 'x2': 332, 'y2': 381 },
{ 'x1': 710, 'y1': 783, 'x2': 272, 'y2': 783 },
{ 'x1': 505, 'y1': 863, 'x2': 240, 'y2': 863 },
{ 'x1': 146, 'y1': 548, 'x2': 146, 'y2': 849 },
{ 'x1': 71, 'y1': 822, 'x2': 820, 'y2': 73 },
{ 'x1': 875, 'y1': 883, 'x2': 451, 'y2': 883 },
{ 'x1': 983, 'y1': 17, 'x2': 30, 'y2': 970 },
{ 'x1': 646, 'y1': 350, 'x2': 646, 'y2': 330 },
{ 'x1': 618, 'y1': 430, 'x2': 881, 'y2': 430 },
{ 'x1': 398, 'y1': 977, 'x2': 643, 'y2': 732 },
{ 'x1': 839, 'y1': 441, 'x2': 406, 'y2': 874 },
{ 'x1': 948, 'y1': 358, 'x2': 178, 'y2': 358 },
{ 'x1': 26, 'y1': 660, 'x2': 26, 'y2': 612 },
{ 'x1': 418, 'y1': 467, 'x2': 418, 'y2': 115 },
{ 'x1': 899, 'y1': 936, 'x2': 899, 'y2': 611 },
{ 'x1': 770, 'y1': 430, 'x2': 770, 'y2': 648 },
{ 'x1': 653, 'y1': 136, 'x2': 95, 'y2': 694 },
{ 'x1': 645, 'y1': 153, 'x2': 818, 'y2': 153 },
{ 'x1': 622, 'y1': 604, 'x2': 77, 'y2': 59 },
{ 'x1': 470, 'y1': 597, 'x2': 592, 'y2': 719 },
{ 'x1': 753, 'y1': 921, 'x2': 743, 'y2': 921 },
{ 'x1': 507, 'y1': 821, 'x2': 507, 'y2': 275 },
{ 'x1': 696, 'y1': 969, 'x2': 433, 'y2': 969 },
{ 'x1': 48, 'y1': 284, 'x2': 552, 'y2': 284 },
{ 'x1': 391, 'y1': 866, 'x2': 187, 'y2': 866 },
{ 'x1': 571, 'y1': 897, 'x2': 107, 'y2': 433 },
{ 'x1': 24, 'y1': 466, 'x2': 24, 'y2': 186 },
{ 'x1': 949, 'y1': 587, 'x2': 582, 'y2': 587 },
{ 'x1': 541, 'y1': 32, 'x2': 541, 'y2': 583 },
{ 'x1': 12, 'y1': 984, 'x2': 986, 'y2': 10 },
{ 'x1': 124, 'y1': 988, 'x2': 124, 'y2': 904 },
{ 'x1': 209, 'y1': 548, 'x2': 209, 'y2': 12 },
{ 'x1': 828, 'y1': 652, 'x2': 34, 'y2': 652 },
{ 'x1': 455, 'y1': 460, 'x2': 905, 'y2': 910 },
{ 'x1': 632, 'y1': 638, 'x2': 343, 'y2': 638 },
{ 'x1': 371, 'y1': 275, 'x2': 145, 'y2': 275 },
{ 'x1': 66, 'y1': 941, 'x2': 945, 'y2': 62 },
{ 'x1': 672, 'y1': 286, 'x2': 672, 'y2': 301 },
{ 'x1': 447, 'y1': 45, 'x2': 82, 'y2': 410 },
{ 'x1': 665, 'y1': 683, 'x2': 487, 'y2': 683 },
{ 'x1': 410, 'y1': 582, 'x2': 670, 'y2': 842 },
{ 'x1': 917, 'y1': 705, 'x2': 917, 'y2': 323 },
{ 'x1': 122, 'y1': 372, 'x2': 403, 'y2': 653 },
{ 'x1': 970, 'y1': 47, 'x2': 40, 'y2': 977 },
{ 'x1': 15, 'y1': 707, 'x2': 732, 'y2': 707 },
{ 'x1': 666, 'y1': 854, 'x2': 666, 'y2': 256 },
{ 'x1': 640, 'y1': 230, 'x2': 188, 'y2': 230 },
{ 'x1': 71, 'y1': 297, 'x2': 71, 'y2': 472 },
{ 'x1': 51, 'y1': 431, 'x2': 339, 'y2': 431 },
{ 'x1': 957, 'y1': 363, 'x2': 82, 'y2': 363 },
{ 'x1': 844, 'y1': 171, 'x2': 274, 'y2': 171 },
{ 'x1': 184, 'y1': 81, 'x2': 184, 'y2': 786 },
{ 'x1': 437, 'y1': 456, 'x2': 138, 'y2': 456 },
{ 'x1': 326, 'y1': 478, 'x2': 814, 'y2': 966 },
{ 'x1': 956, 'y1': 76, 'x2': 226, 'y2': 806 },
{ 'x1': 399, 'y1': 572, 'x2': 399, 'y2': 877 },
{ 'x1': 923, 'y1': 813, 'x2': 923, 'y2': 898 },
{ 'x1': 445, 'y1': 221, 'x2': 947, 'y2': 723 },
{ 'x1': 85, 'y1': 590, 'x2': 415, 'y2': 590 },
{ 'x1': 644, 'y1': 127, 'x2': 644, 'y2': 140 },
{ 'x1': 372, 'y1': 732, 'x2': 372, 'y2': 522 },
{ 'x1': 413, 'y1': 186, 'x2': 244, 'y2': 186 },
{ 'x1': 291, 'y1': 163, 'x2': 197, 'y2': 69 },
{ 'x1': 889, 'y1': 41, 'x2': 208, 'y2': 722 },
{ 'x1': 835, 'y1': 925, 'x2': 835, 'y2': 616 },
{ 'x1': 790, 'y1': 340, 'x2': 426, 'y2': 340 },
{ 'x1': 26, 'y1': 872, 'x2': 805, 'y2': 93 },
{ 'x1': 617, 'y1': 701, 'x2': 828, 'y2': 701 },
{ 'x1': 344, 'y1': 654, 'x2': 530, 'y2': 654 },
{ 'x1': 940, 'y1': 989, 'x2': 41, 'y2': 90 },
{ 'x1': 642, 'y1': 87, 'x2': 206, 'y2': 523 },
{ 'x1': 306, 'y1': 54, 'x2': 306, 'y2': 761 },
{ 'x1': 672, 'y1': 778, 'x2': 672, 'y2': 868 },
{ 'x1': 29, 'y1': 959, 'x2': 974, 'y2': 14 },
{ 'x1': 458, 'y1': 166, 'x2': 86, 'y2': 166 },
{ 'x1': 565, 'y1': 394, 'x2': 352, 'y2': 181 },
{ 'x1': 516, 'y1': 805, 'x2': 457, 'y2': 805 },
{ 'x1': 464, 'y1': 450, 'x2': 756, 'y2': 450 },
{ 'x1': 177, 'y1': 181, 'x2': 661, 'y2': 181 },
{ 'x1': 962, 'y1': 394, 'x2': 630, 'y2': 62 },
{ 'x1': 94, 'y1': 613, 'x2': 12, 'y2': 613 },
{ 'x1': 932, 'y1': 159, 'x2': 818, 'y2': 273 },
{ 'x1': 640, 'y1': 642, 'x2': 976, 'y2': 642 },
{ 'x1': 184, 'y1': 346, 'x2': 184, 'y2': 22 },
{ 'x1': 370, 'y1': 672, 'x2': 458, 'y2': 672 },
{ 'x1': 235, 'y1': 334, 'x2': 770, 'y2': 869 },
{ 'x1': 207, 'y1': 17, 'x2': 811, 'y2': 17 },
{ 'x1': 351, 'y1': 25, 'x2': 896, 'y2': 25 },
{ 'x1': 589, 'y1': 178, 'x2': 921, 'y2': 510 },
{ 'x1': 672, 'y1': 733, 'x2': 937, 'y2': 733 },
{ 'x1': 159, 'y1': 718, 'x2': 787, 'y2': 90 },
{ 'x1': 598, 'y1': 317, 'x2': 598, 'y2': 455 },
{ 'x1': 591, 'y1': 164, 'x2': 591, 'y2': 374 },
{ 'x1': 276, 'y1': 321, 'x2': 515, 'y2': 321 },
{ 'x1': 245, 'y1': 431, 'x2': 245, 'y2': 350 },
{ 'x1': 268, 'y1': 976, 'x2': 939, 'y2': 305 },
{ 'x1': 319, 'y1': 297, 'x2': 823, 'y2': 297 },
{ 'x1': 246, 'y1': 132, 'x2': 229, 'y2': 132 },
{ 'x1': 338, 'y1': 707, 'x2': 338, 'y2': 59 },
{ 'x1': 723, 'y1': 402, 'x2': 723, 'y2': 739 },
{ 'x1': 539, 'y1': 846, 'x2': 94, 'y2': 846 },
{ 'x1': 879, 'y1': 458, 'x2': 783, 'y2': 458 },
{ 'x1': 400, 'y1': 847, 'x2': 770, 'y2': 477 },
{ 'x1': 111, 'y1': 957, 'x2': 858, 'y2': 210 },
{ 'x1': 640, 'y1': 340, 'x2': 878, 'y2': 578 },
{ 'x1': 613, 'y1': 446, 'x2': 115, 'y2': 446 },
{ 'x1': 119, 'y1': 480, 'x2': 119, 'y2': 425 },
{ 'x1': 447, 'y1': 34, 'x2': 986, 'y2': 34 },
{ 'x1': 730, 'y1': 357, 'x2': 532, 'y2': 555 },
{ 'x1': 85, 'y1': 283, 'x2': 26, 'y2': 283 },
{ 'x1': 834, 'y1': 327, 'x2': 263, 'y2': 898 },
{ 'x1': 739, 'y1': 580, 'x2': 640, 'y2': 481 },
{ 'x1': 841, 'y1': 938, 'x2': 841, 'y2': 123 },
{ 'x1': 987, 'y1': 979, 'x2': 23, 'y2': 15 },
{ 'x1': 827, 'y1': 563, 'x2': 632, 'y2': 563 },
{ 'x1': 313, 'y1': 419, 'x2': 466, 'y2': 572 },
{ 'x1': 323, 'y1': 326, 'x2': 455, 'y2': 326 },
{ 'x1': 976, 'y1': 683, 'x2': 976, 'y2': 314 },
{ 'x1': 896, 'y1': 568, 'x2': 338, 'y2': 568 },
{ 'x1': 733, 'y1': 464, 'x2': 819, 'y2': 464 },
{ 'x1': 613, 'y1': 608, 'x2': 493, 'y2': 608 },
{ 'x1': 178, 'y1': 137, 'x2': 178, 'y2': 663 },
{ 'x1': 262, 'y1': 595, 'x2': 262, 'y2': 308 },
{ 'x1': 535, 'y1': 268, 'x2': 851, 'y2': 268 },
{ 'x1': 653, 'y1': 111, 'x2': 653, 'y2': 723 },
{ 'x1': 947, 'y1': 47, 'x2': 23, 'y2': 971 },
{ 'x1': 351, 'y1': 800, 'x2': 351, 'y2': 806 },
{ 'x1': 143, 'y1': 49, 'x2': 850, 'y2': 756 },
{ 'x1': 872, 'y1': 643, 'x2': 872, 'y2': 265 },
{ 'x1': 946, 'y1': 727, 'x2': 427, 'y2': 727 },
{ 'x1': 354, 'y1': 284, 'x2': 938, 'y2': 868 },
{ 'x1': 652, 'y1': 874, 'x2': 652, 'y2': 447 },
{ 'x1': 920, 'y1': 27, 'x2': 387, 'y2': 27 },
{ 'x1': 816, 'y1': 265, 'x2': 816, 'y2': 366 },
{ 'x1': 238, 'y1': 632, 'x2': 336, 'y2': 534 },
{ 'x1': 593, 'y1': 399, 'x2': 593, 'y2': 425 },
{ 'x1': 696, 'y1': 108, 'x2': 138, 'y2': 666 },
{ 'x1': 828, 'y1': 110, 'x2': 240, 'y2': 698 },
{ 'x1': 421, 'y1': 590, 'x2': 847, 'y2': 590 },
{ 'x1': 409, 'y1': 449, 'x2': 622, 'y2': 449 },
{ 'x1': 986, 'y1': 291, 'x2': 45, 'y2': 291 },
{ 'x1': 580, 'y1': 34, 'x2': 663, 'y2': 34 },
{ 'x1': 921, 'y1': 988, 'x2': 35, 'y2': 102 },
{ 'x1': 619, 'y1': 88, 'x2': 249, 'y2': 88 },
{ 'x1': 48, 'y1': 773, 'x2': 503, 'y2': 773 },
{ 'x1': 277, 'y1': 560, 'x2': 377, 'y2': 460 },
{ 'x1': 139, 'y1': 803, 'x2': 123, 'y2': 787 },
{ 'x1': 339, 'y1': 688, 'x2': 907, 'y2': 120 },
{ 'x1': 775, 'y1': 117, 'x2': 775, 'y2': 203 },
{ 'x1': 237, 'y1': 287, 'x2': 507, 'y2': 557 },
{ 'x1': 27, 'y1': 832, 'x2': 429, 'y2': 430 },
{ 'x1': 70, 'y1': 592, 'x2': 672, 'y2': 592 },
{ 'x1': 701, 'y1': 295, 'x2': 69, 'y2': 295 },
{ 'x1': 723, 'y1': 672, 'x2': 714, 'y2': 672 },
{ 'x1': 974, 'y1': 359, 'x2': 292, 'y2': 359 },
{ 'x1': 439, 'y1': 462, 'x2': 439, 'y2': 973 },
{ 'x1': 681, 'y1': 922, 'x2': 357, 'y2': 922 },
{ 'x1': 402, 'y1': 521, 'x2': 25, 'y2': 521 },
{ 'x1': 297, 'y1': 525, 'x2': 297, 'y2': 227 },
{ 'x1': 763, 'y1': 797, 'x2': 763, 'y2': 153 },
{ 'x1': 737, 'y1': 634, 'x2': 869, 'y2': 634 },
{ 'x1': 379, 'y1': 875, 'x2': 810, 'y2': 875 },
{ 'x1': 427, 'y1': 117, 'x2': 705, 'y2': 117 },
{ 'x1': 785, 'y1': 376, 'x2': 265, 'y2': 896 },
{ 'x1': 922, 'y1': 818, 'x2': 141, 'y2': 37 },
{ 'x1': 61, 'y1': 371, 'x2': 61, 'y2': 432 },
{ 'x1': 557, 'y1': 303, 'x2': 411, 'y2': 303 },
{ 'x1': 573, 'y1': 100, 'x2': 295, 'y2': 100 },
{ 'x1': 767, 'y1': 657, 'x2': 231, 'y2': 657 },
{ 'x1': 11, 'y1': 938, 'x2': 875, 'y2': 74 },
{ 'x1': 782, 'y1': 393, 'x2': 782, 'y2': 502 },
{ 'x1': 899, 'y1': 913, 'x2': 519, 'y2': 533 },
{ 'x1': 887, 'y1': 11, 'x2': 244, 'y2': 654 },
{ 'x1': 730, 'y1': 271, 'x2': 426, 'y2': 271 },
{ 'x1': 555, 'y1': 538, 'x2': 379, 'y2': 538 },
{ 'x1': 871, 'y1': 866, 'x2': 871, 'y2': 461 },
{ 'x1': 934, 'y1': 178, 'x2': 934, 'y2': 845 },
{ 'x1': 741, 'y1': 327, 'x2': 325, 'y2': 327 },
{ 'x1': 332, 'y1': 394, 'x2': 518, 'y2': 580 },
{ 'x1': 673, 'y1': 372, 'x2': 809, 'y2': 236 },
{ 'x1': 951, 'y1': 972, 'x2': 77, 'y2': 98 },
{ 'x1': 985, 'y1': 585, 'x2': 985, 'y2': 55 },
{ 'x1': 604, 'y1': 341, 'x2': 269, 'y2': 676 },
{ 'x1': 408, 'y1': 70, 'x2': 455, 'y2': 23 },
{ 'x1': 718, 'y1': 239, 'x2': 718, 'y2': 522 },
{ 'x1': 15, 'y1': 16, 'x2': 984, 'y2': 985 },
{ 'x1': 743, 'y1': 836, 'x2': 852, 'y2': 836 },
{ 'x1': 407, 'y1': 59, 'x2': 246, 'y2': 220 },
{ 'x1': 388, 'y1': 538, 'x2': 388, 'y2': 587 },
{ 'x1': 978, 'y1': 249, 'x2': 810, 'y2': 249 },
{ 'x1': 506, 'y1': 945, 'x2': 506, 'y2': 644 },
{ 'x1': 657, 'y1': 101, 'x2': 657, 'y2': 522 },
{ 'x1': 879, 'y1': 261, 'x2': 935, 'y2': 261 },
{ 'x1': 751, 'y1': 625, 'x2': 926, 'y2': 800 },
{ 'x1': 676, 'y1': 183, 'x2': 573, 'y2': 286 },
{ 'x1': 272, 'y1': 491, 'x2': 765, 'y2': 984 },
{ 'x1': 867, 'y1': 207, 'x2': 570, 'y2': 207 },
{ 'x1': 117, 'y1': 784, 'x2': 750, 'y2': 784 },
{ 'x1': 883, 'y1': 528, 'x2': 883, 'y2': 845 },
{ 'x1': 430, 'y1': 765, 'x2': 581, 'y2': 614 },
{ 'x1': 748, 'y1': 839, 'x2': 748, 'y2': 775 },
{ 'x1': 265, 'y1': 16, 'x2': 188, 'y2': 93 },
{ 'x1': 894, 'y1': 584, 'x2': 525, 'y2': 215 },
{ 'x1': 278, 'y1': 414, 'x2': 19, 'y2': 673 },
{ 'x1': 588, 'y1': 446, 'x2': 588, 'y2': 656 },
{ 'x1': 469, 'y1': 676, 'x2': 469, 'y2': 354 },
{ 'x1': 383, 'y1': 848, 'x2': 669, 'y2': 848 },
{ 'x1': 212, 'y1': 341, 'x2': 367, 'y2': 341 },
{ 'x1': 988, 'y1': 582, 'x2': 988, 'y2': 825 },
{ 'x1': 296, 'y1': 187, 'x2': 785, 'y2': 187 },
{ 'x1': 255, 'y1': 400, 'x2': 255, 'y2': 286 },
{ 'x1': 756, 'y1': 886, 'x2': 524, 'y2': 654 },
{ 'x1': 966, 'y1': 32, 'x2': 966, 'y2': 940 },
{ 'x1': 711, 'y1': 243, 'x2': 81, 'y2': 873 },
{ 'x1': 207, 'y1': 34, 'x2': 359, 'y2': 186 },
{ 'x1': 555, 'y1': 850, 'x2': 45, 'y2': 850 },
{ 'x1': 41, 'y1': 962, 'x2': 947, 'y2': 56 },
{ 'x1': 15, 'y1': 888, 'x2': 15, 'y2': 795 },
{ 'x1': 773, 'y1': 709, 'x2': 579, 'y2': 515 },
{ 'x1': 228, 'y1': 517, 'x2': 228, 'y2': 334 },
{ 'x1': 495, 'y1': 290, 'x2': 965, 'y2': 290 },
{ 'x1': 462, 'y1': 309, 'x2': 462, 'y2': 421 },
{ 'x1': 945, 'y1': 274, 'x2': 945, 'y2': 735 },
{ 'x1': 547, 'y1': 361, 'x2': 547, 'y2': 140 },
{ 'x1': 37, 'y1': 930, 'x2': 836, 'y2': 930 },
{ 'x1': 987, 'y1': 445, 'x2': 45, 'y2': 445 },
{ 'x1': 25, 'y1': 766, 'x2': 757, 'y2': 34 },
{ 'x1': 643, 'y1': 946, 'x2': 318, 'y2': 621 },
{ 'x1': 507, 'y1': 402, 'x2': 507, 'y2': 981 },
{ 'x1': 22, 'y1': 903, 'x2': 488, 'y2': 437 },
{ 'x1': 887, 'y1': 487, 'x2': 957, 'y2': 487 },
{ 'x1': 57, 'y1': 434, 'x2': 21, 'y2': 398 },
{ 'x1': 688, 'y1': 907, 'x2': 917, 'y2': 678 },
{ 'x1': 146, 'y1': 498, 'x2': 146, 'y2': 965 },
{ 'x1': 375, 'y1': 939, 'x2': 888, 'y2': 426 },
{ 'x1': 966, 'y1': 151, 'x2': 684, 'y2': 433 },
{ 'x1': 160, 'y1': 373, 'x2': 722, 'y2': 373 },
{ 'x1': 410, 'y1': 571, 'x2': 410, 'y2': 683 },
{ 'x1': 588, 'y1': 667, 'x2': 588, 'y2': 337 },
{ 'x1': 832, 'y1': 348, 'x2': 376, 'y2': 804 },
{ 'x1': 567, 'y1': 430, 'x2': 453, 'y2': 430 },
{ 'x1': 368, 'y1': 442, 'x2': 97, 'y2': 442 },
{ 'x1': 11, 'y1': 191, 'x2': 584, 'y2': 764 },
{ 'x1': 647, 'y1': 965, 'x2': 647, 'y2': 331 },
{ 'x1': 576, 'y1': 856, 'x2': 976, 'y2': 456 },
{ 'x1': 205, 'y1': 287, 'x2': 205, 'y2': 495 },
{ 'x1': 677, 'y1': 872, 'x2': 677, 'y2': 246 },
{ 'x1': 21, 'y1': 16, 'x2': 973, 'y2': 968 },
{ 'x1': 949, 'y1': 738, 'x2': 126, 'y2': 738 },
{ 'x1': 921, 'y1': 595, 'x2': 921, 'y2': 370 },
{ 'x1': 539, 'y1': 821, 'x2': 660, 'y2': 821 },
{ 'x1': 143, 'y1': 188, 'x2': 143, 'y2': 793 },
{ 'x1': 639, 'y1': 930, 'x2': 639, 'y2': 454 },
{ 'x1': 522, 'y1': 360, 'x2': 917, 'y2': 755 },
{ 'x1': 128, 'y1': 281, 'x2': 647, 'y2': 800 },
{ 'x1': 456, 'y1': 365, 'x2': 456, 'y2': 34 },
{ 'x1': 434, 'y1': 530, 'x2': 431, 'y2': 530 },
{ 'x1': 549, 'y1': 792, 'x2': 549, 'y2': 892 },
{ 'x1': 414, 'y1': 229, 'x2': 41, 'y2': 602 },
{ 'x1': 289, 'y1': 468, 'x2': 592, 'y2': 771 },
{ 'x1': 555, 'y1': 624, 'x2': 727, 'y2': 452 },
{ 'x1': 187, 'y1': 780, 'x2': 187, 'y2': 838 },
{ 'x1': 807, 'y1': 282, 'x2': 807, 'y2': 968 },
{ 'x1': 52, 'y1': 249, 'x2': 52, 'y2': 332 },
{ 'x1': 797, 'y1': 525, 'x2': 462, 'y2': 190 },
{ 'x1': 295, 'y1': 562, 'x2': 654, 'y2': 921 },
{ 'x1': 862, 'y1': 627, 'x2': 252, 'y2': 627 },
{ 'x1': 22, 'y1': 116, 'x2': 866, 'y2': 960 },
{ 'x1': 155, 'y1': 652, 'x2': 155, 'y2': 979 },
{ 'x1': 598, 'y1': 748, 'x2': 51, 'y2': 748 },
{ 'x1': 130, 'y1': 601, 'x2': 231, 'y2': 601 },
{ 'x1': 468, 'y1': 183, 'x2': 468, 'y2': 414 },
{ 'x1': 208, 'y1': 193, 'x2': 927, 'y2': 912 },
{ 'x1': 25, 'y1': 755, 'x2': 770, 'y2': 755 },
{ 'x1': 667, 'y1': 755, 'x2': 916, 'y2': 506 },
{ 'x1': 395, 'y1': 103, 'x2': 395, 'y2': 468 },
{ 'x1': 763, 'y1': 62, 'x2': 763, 'y2': 153 },
{ 'x1': 371, 'y1': 816, 'x2': 371, 'y2': 109 },
{ 'x1': 508, 'y1': 138, 'x2': 37, 'y2': 609 },
{ 'x1': 464, 'y1': 983, 'x2': 707, 'y2': 983 },
{ 'x1': 282, 'y1': 295, 'x2': 282, 'y2': 536 },
{ 'x1': 646, 'y1': 680, 'x2': 646, 'y2': 775 },
{ 'x1': 95, 'y1': 117, 'x2': 95, 'y2': 62 },
{ 'x1': 606, 'y1': 963, 'x2': 203, 'y2': 963 },
{ 'x1': 403, 'y1': 195, 'x2': 97, 'y2': 195 },
{ 'x1': 743, 'y1': 293, 'x2': 743, 'y2': 815 },
{ 'x1': 243, 'y1': 748, 'x2': 431, 'y2': 748 },
{ 'x1': 953, 'y1': 54, 'x2': 22, 'y2': 985 },
{ 'x1': 693, 'y1': 810, 'x2': 656, 'y2': 810 },
{ 'x1': 163, 'y1': 221, 'x2': 728, 'y2': 786 },
{ 'x1': 187, 'y1': 648, 'x2': 187, 'y2': 337 },
{ 'x1': 98, 'y1': 559, 'x2': 98, 'y2': 165 },
{ 'x1': 903, 'y1': 49, 'x2': 46, 'y2': 906 },
{ 'x1': 600, 'y1': 779, 'x2': 432, 'y2': 779 },
{ 'x1': 500, 'y1': 606, 'x2': 500, 'y2': 878 },
{ 'x1': 812, 'y1': 797, 'x2': 31, 'y2': 16 },
{ 'x1': 204, 'y1': 786, 'x2': 517, 'y2': 786 },
{ 'x1': 952, 'y1': 757, 'x2': 952, 'y2': 923 },
{ 'x1': 66, 'y1': 809, 'x2': 66, 'y2': 831 },
{ 'x1': 479, 'y1': 72, 'x2': 17, 'y2': 72 },
{ 'x1': 96, 'y1': 430, 'x2': 96, 'y2': 932 },
{ 'x1': 346, 'y1': 667, 'x2': 796, 'y2': 667 },
{ 'x1': 731, 'y1': 102, 'x2': 162, 'y2': 102 },
{ 'x1': 755, 'y1': 846, 'x2': 755, 'y2': 135 },
{ 'x1': 90, 'y1': 18, 'x2': 404, 'y2': 18 },
{ 'x1': 828, 'y1': 802, 'x2': 134, 'y2': 108 },
{ 'x1': 278, 'y1': 946, 'x2': 278, 'y2': 765 },
{ 'x1': 289, 'y1': 163, 'x2': 869, 'y2': 743 },
{ 'x1': 134, 'y1': 58, 'x2': 887, 'y2': 58 },
{ 'x1': 354, 'y1': 735, 'x2': 904, 'y2': 185 },
{ 'x1': 897, 'y1': 74, 'x2': 897, 'y2': 716 },
{ 'x1': 508, 'y1': 904, 'x2': 508, 'y2': 719 },
{ 'x1': 285, 'y1': 358, 'x2': 795, 'y2': 868 },
{ 'x1': 51, 'y1': 155, 'x2': 51, 'y2': 643 },
{ 'x1': 579, 'y1': 388, 'x2': 431, 'y2': 240 },
{ 'x1': 79, 'y1': 330, 'x2': 869, 'y2': 330 },
{ 'x1': 955, 'y1': 863, 'x2': 140, 'y2': 48 },
{ 'x1': 800, 'y1': 473, 'x2': 689, 'y2': 473 },
{ 'x1': 711, 'y1': 128, 'x2': 918, 'y2': 128 },
{ 'x1': 787, 'y1': 166, 'x2': 787, 'y2': 36 },
{ 'x1': 257, 'y1': 684, 'x2': 873, 'y2': 68 },
{ 'x1': 984, 'y1': 986, 'x2': 10, 'y2': 12 },
{ 'x1': 76, 'y1': 580, 'x2': 660, 'y2': 580 },
{ 'x1': 265, 'y1': 868, 'x2': 955, 'y2': 868 },
{ 'x1': 569, 'y1': 491, 'x2': 466, 'y2': 594 },
{ 'x1': 535, 'y1': 654, 'x2': 37, 'y2': 654 },
{ 'x1': 889, 'y1': 751, 'x2': 411, 'y2': 273 },
{ 'x1': 681, 'y1': 491, 'x2': 600, 'y2': 491 },
{ 'x1': 459, 'y1': 721, 'x2': 459, 'y2': 109 },
{ 'x1': 824, 'y1': 130, 'x2': 689, 'y2': 265 },
{ 'x1': 406, 'y1': 780, 'x2': 406, 'y2': 965 },
{ 'x1': 18, 'y1': 31, 'x2': 904, 'y2': 917 },
{ 'x1': 179, 'y1': 91, 'x2': 232, 'y2': 38 },
{ 'x1': 280, 'y1': 552, 'x2': 428, 'y2': 700 },
{ 'x1': 967, 'y1': 980, 'x2': 19, 'y2': 32 },
{ 'x1': 47, 'y1': 289, 'x2': 738, 'y2': 980 },
{ 'x1': 160, 'y1': 592, 'x2': 120, 'y2': 592 },
{ 'x1': 710, 'y1': 412, 'x2': 28, 'y2': 412 },
{ 'x1': 40, 'y1': 430, 'x2': 747, 'y2': 430 },
{ 'x1': 435, 'y1': 821, 'x2': 747, 'y2': 509 },
{ 'x1': 36, 'y1': 741, 'x2': 36, 'y2': 363 },
{ 'x1': 787, 'y1': 30, 'x2': 847, 'y2': 30 },
{ 'x1': 725, 'y1': 968, 'x2': 725, 'y2': 940 },
{ 'x1': 739, 'y1': 356, 'x2': 27, 'y2': 356 },
{ 'x1': 65, 'y1': 193, 'x2': 419, 'y2': 193 },
{ 'x1': 318, 'y1': 513, 'x2': 404, 'y2': 513 },
{ 'x1': 150, 'y1': 230, 'x2': 150, 'y2': 968 },
{ 'x1': 847, 'y1': 410, 'x2': 847, 'y2': 505 },
{ 'x1': 950, 'y1': 188, 'x2': 950, 'y2': 278 },
{ 'x1': 816, 'y1': 312, 'x2': 587, 'y2': 541 },
{ 'x1': 980, 'y1': 145, 'x2': 154, 'y2': 145 },
{ 'x1': 79, 'y1': 781, 'x2': 616, 'y2': 781 },
{ 'x1': 433, 'y1': 284, 'x2': 433, 'y2': 309 },
{ 'x1': 624, 'y1': 264, 'x2': 569, 'y2': 264 },
{ 'x1': 604, 'y1': 859, 'x2': 570, 'y2': 825 },
{ 'x1': 936, 'y1': 691, 'x2': 350, 'y2': 105 },
{ 'x1': 259, 'y1': 631, 'x2': 648, 'y2': 242 },
{ 'x1': 445, 'y1': 27, 'x2': 445, 'y2': 872 },
{ 'x1': 438, 'y1': 689, 'x2': 977, 'y2': 150 },
{ 'x1': 718, 'y1': 603, 'x2': 718, 'y2': 327 },
{ 'x1': 967, 'y1': 310, 'x2': 98, 'y2': 310 },
{ 'x1': 439, 'y1': 381, 'x2': 439, 'y2': 35 },
{ 'x1': 645, 'y1': 240, 'x2': 282, 'y2': 240 },
{ 'x1': 475, 'y1': 54, 'x2': 475, 'y2': 658 },
{ 'x1': 972, 'y1': 610, 'x2': 759, 'y2': 823 }
];

resolve();