console.log("Solution for day 6/12/2021");
console.log("https://adventofcode.com/2021/day/6");
console.log("");

class Lanternfish
{
    constructor(count, cicle)
    {
        this.count = count;
        this.cicle = cicle;
    }

    update()
    {
        this.cicle -= 1;

        if(this.cicle < 0) {
            this.cicle = 6;
            return true;
        }

        return false;
    }
}

function resolve()
{
    let fishCounter = {};
    INPUT.forEach(fishCicle => {
        if(fishCounter[fishCicle])
            fishCounter[fishCicle] += 1;
        else
            fishCounter[fishCicle] = 1;
    });

    let fishList = [];
    Object.keys(fishCounter).forEach(fishCicle => {
        fishList.push(new Lanternfish(fishCounter[fishCicle], fishCicle))
    });

    //Simulate 80 cicles
    for(let i = 0; i < 80; i++)
    {
        simulateCicle(fishList);
    }

    console.log(`SOLUTION FOR 6.1: ${countFish(fishList)}`);
    //353079

    //Simulate 256 cicles
    for(let i = 80; i < 256; i++)
    {
        simulateCicle(fishList);
    }

    console.log(`SOLUTION FOR 6.2: ${countFish(fishList)}`);
    //1605400130036
}

function simulateCicle(fishList)
{
    let newOnes = 0;
    fishList.forEach(fish =>
    {
        if (fish.update())
            newOnes += fish.count;
    });

    if(newOnes > 0)
        fishList.push(new Lanternfish(newOnes, 8));
}

function countFish(fishList)
{
    let count = 0;
    fishList.forEach(fish => {
        count += fish.count;
    })
    return count;
}

const INPUT = [
    4,5,3,2,3,3,2,4,2,1,2,4,5,2,2,2,4,1,1,1,5,1,1,2,5,2,1,1,4,4,5,5,1,2,1,1,5,3,5,2,4,3,2,4,5,3,2,1,4,1,3,1,2,4,1,1,4,1,4,2,5,1,4,3,5,2,4,5,4,2,2,5,1,1,2,4,1,4,4,1,1,3,1,2,3,2,5,5,1,1,5,2,4,2,2,4,1,1,1,4,2,2,3,1,2,4,5,4,5,4,2,3,1,4,1,3,1,2,3,3,2,4,3,3,3,1,4,2,3,4,2,1,5,4,2,4,4,3,2,1,5,3,1,4,1,1,5,4,2,4,2,2,4,4,4,1,4,2,4,1,1,3,5,1,5,5,1,3,2,2,3,5,3,1,1,4,4,1,3,3,3,5,1,1,2,5,5,5,2,4,1,5,1,2,1,1,1,4,3,1,5,2,3,1,3,1,4,1,3,5,4,5,1,3,4,2,1,5,1,3,4,5,5,2,1,2,1,1,1,4,3,1,4,2,3,1,3,5,1,4,5,3,1,3,3,2,2,1,5,5,4,3,2,1,5,1,3,1,3,5,1,1,2,1,1,1,5,2,1,1,3,2,1,5,5,5,1,1,5,1,4,1,5,4,2,4,5,2,4,3,2,5,4,1,1,2,4,3,2,1
];

resolve();